<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/*Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');
*/
Route::middleware('auth')->group(function () {
    // Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    // Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    // Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/profile/courses', [ProfileController::class, 'courses'])->name('profile.courses')->middleware(['auth','student']);
});
Route::controller(CourseController::class)->group(function(){
    Route::get('/', 'listing')->name('courses');
    Route::get('/courses', 'listing')->name('courses');
    Route::get('/courses/create', function(){
        return view('courses.create');
    })->name('courses-create-form')->middleware('admin');
    Route::post('/course/create', 'create')->name('course-create')->middleware('admin');
    Route::get('/course/{id}/enroll', 'enroll')->name('course-enroll')->middleware(['auth', 'student']);
    Route::get('/course/{id}/unenroll', 'unenroll')->name('course-unenroll')->middleware(['auth', 'student']);
    Route::get('/course/{id}/edit', 'editForm')->name('course-edit-form')->middleware(['auth', 'admin']);
    Route::post('/course/{id}/edit', 'edit')->name('course-edit')->middleware(['auth', 'admin']);
    Route::post('/course/update', 'update')->name('course-update')->middleware(['auth', 'admin']);
    Route::delete('/course/{id}/delete', 'delete')->name('course-delete')->middleware(['auth', 'admin']);
});

Route::get('/about', AboutController::class)->name('about');

Route::controller(ContactController::class)->group(function(){
    Route::get('/contact', 'index')->name('contact');
    Route::post('/contact/send-email', 'sendEmail')->name('send-email');
});


require __DIR__.'/auth.php';
