<p 
    {!! $attributes->merge(['class' => 'mt-6 text-lg leading-8 text-gray-600']) !!}
>
{{ $slot }}
</p>