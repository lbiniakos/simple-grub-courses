@props([
    'course'
])

<li class="flex justify-between gap-x-6 py-5">

    <div class="flex min-w-0 gap-x-4">
      <img class="h-12 w-12 flex-none rounded-full bg-gray-50" src="https://images.unsplash.com/photo-1591951425328-48c1fe7179cd?q=256&w=256&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="">
      <div class="min-w-0 flex-auto">
        <p class="text-sm font-semibold leading-6 text-gray-900">{{ $course->title }}</p>
        <p class="mt-1 truncate text-xs leading-5 text-gray-500">{{ $course->description }}</p>
      </div>
    </div>

    <div class="hidden shrink-0 sm:flex sm:flex-col sm:items-end">
        <x-course-slots-available>{{ $course->enrolls }} / {{ $course->total_slots }}</x-course-slots-available>
        <div class="flex items-center justify-center gap-x-6 lg:justify-start">

          @guest
          <x-nav-link class="inline-flex items-center rounded-md mt-1 text-xs font-medium bg-gray-50 text-gray-600 ring-1 ring-inset ring-gray-500/10" href="{{ route('login') }}">{{ __('Login to enroll') }}</x-nav-link>
          @endguest

          @auth
          <x-course-status :status="($course->status == 1) ? __('Available') : __('Not Available')" :statusColor="( $course->status == 1) ? 'green' : 'red'"/>
            @if ($course->status == 1 && auth()->user()?->role == 1)
            <x-nav-link class="bg-indigo-600 text-white" :href="route('course-enroll',['id' => $course->id])">{{ __('Enroll') }}</x-nav-link>
            @endif

            @if (auth()->user()?->role == 0)
            <x-nav-link class="bg-yellow-50 px-2 py-1 text-xs font-medium text-yellow-800 ring-1 ring-inset ring-yellow-600/20" :href="route('course-edit',['id' => $course->id])">{{ __('Edit') }}</x-nav-link>

            <form method="POST" action="{{ route('course-delete',['id' => $course->id]) }}">
              @csrf
              <x-text-input type="hidden" name="_method" value="DELETE"/>
              <x-primary-button class="bg-red-50 hover:bg-red-500 hover:text-white px-2 py-1 text-xs font-medium text-red-800 ring-1 ring-inset ring-red-600/20">{{ __('Delete') }}</x-primary-button>
            </form>
            @endif
          @endauth

        </div>
        
      <p class="mt-1 text-xs leading-5 text-gray-500">{{ __('Created at:') }} <time datetime="{{ $course->created_at }}">{{ \Carbon\Carbon::parse($course->created_at)->diffForHumans() }}</time></p>
    </div>
</li>