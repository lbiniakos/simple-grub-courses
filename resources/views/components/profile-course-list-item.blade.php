@props([
    'course'
])

<li class="flex justify-between gap-x-6 py-5">
    <div class="flex min-w-0 gap-x-4">
      <img class="h-12 w-12 flex-none rounded-full bg-gray-50" src="https://images.unsplash.com/photo-1591951425328-48c1fe7179cd?q=256&w=256&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="">
      <div class="min-w-0 flex-auto">
        <p class="text-sm font-semibold leading-6 text-gray-900">{{ $course->title }}</p>
        <p class="mt-1 truncate text-xs leading-5 text-gray-500">{{ $course->description }}</p>
      </div>
    </div>
    <div class="hidden shrink-0 sm:flex sm:flex-col sm:items-end">
        <div class="flex items-center justify-center gap-x-6 lg:justify-start">
            <x-nav-link class="bg-red-600 text-white" :href="route('course-unenroll',['id' => $course->id])">{{ __('Leave Course') }}</x-nav-link>
        </div>
        <p class="mt-1 text-xs leading-5 text-gray-500">{{ __('Created at:') }} <time datetime="{{ $course->created_at }}">{{ \Carbon\Carbon::parse($course->created_at)->diffForHumans() }}</time></p>
    </div>
</li>