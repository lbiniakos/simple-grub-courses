<div class="relative isolate px-6 pt-14 lg:px-8">
    <div class="mx-auto max-w-2xl py-10">
      <div class="text-center">
        {{ $slot }}
      </div>
    </div>
</div>