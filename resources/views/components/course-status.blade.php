@props([
    'status',
    'statusColor'
])
<span class="inline-flex items-center rounded-md bg-{{ $statusColor }}-50 px-2 py-1 text-xs font-medium text-{{ $statusColor }}-700 ring-1 ring-inset ring-{{ $statusColor }}-600/20">{{ $status }}</span>