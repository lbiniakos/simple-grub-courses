<nav class="bg-gray-800" x-data="{ open: false }">
    <div class="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        <div class="flex h-16 items-center justify-between">
            <div class="flex items-center">
                <div class="flex-shrink-0">
                    <img class="h-8 w-8" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=500" alt="Your Company">
                </div>
                <div class="hidden md:block">
                    <div class="ml-10 flex items-baseline space-x-4">
                        <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
                        <x-nav-link :href="route('courses')" :active="request()->routeIs('courses')">
                            {{ __('Courses') }}
                        </x-nav-link>
                        <x-nav-link :href="route('about')" :active="request()->routeIs('about')">
                            {{ __('About') }}
                        </x-nav-link>
                        <x-nav-link :href="route('contact')" :active="request()->routeIs('contact')">
                            {{ __('Contact') }}
                        </x-nav-link>
                    </div>
                </div>
            </div>

            @auth
            <div class="hidden md:block">
                <div class="ml-4 flex items-center md:ml-6">
                    <span class="text-white">{{ __('Hi,') }} {{ Auth::user()->name }} !</span>
                    <!-- Profile dropdown -->
                    <div x-data="{ open: false }"
                     class="relative ml-3">
                        <div @click.away="open = false" class="relative">
                            <button 
                                type="button" 
                                class="relative flex rounded-full bg-gray-800 text-sm focus:outline-none focus:ring-2 focus:ring-white focus:ring-offset-2 focus:ring-offset-gray-800" id="user-menu-button"
                                aria-expanded="false" 
                                aria-haspopup="true"
                                @click="open = true"
                            >
                                <span class="absolute -inset-1.5"></span>
                                <span class="sr-only">Open user menu</span>
                                <img class="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                            </button>
                        </div>
                        <div x-show="open" @click.away="open = false" class="absolute right-0 z-10 mt-2 w-48 origin-top-right rounded-md bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none" role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1">
                            @if ( auth()->user()->role == 0)
                            <x-responsive-nav-link class="block rounded-md px-3 py-2 text-base font-medium text-gray-400 hover:bg-gray-700 hover:text-white" :href="route('courses-create-form')">
                                {{ __('Create Courses') }}
                            </x-responsive-nav-link>
                            @endif

                            @if ( auth()->user()->role == 1)
                            <x-responsive-nav-link class="block rounded-md px-3 py-2 text-base font-medium text-gray-400 hover:bg-gray-700 hover:text-white" :href="route('profile.courses')">
                                {{ __('My Courses') }}
                            </x-responsive-nav-link>
                            @endif

                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                
                                <x-responsive-nav-link class="block rounded-md px-3 py-2 text-base font-medium text-gray-400 hover:bg-gray-700 hover:text-white" :href="route('logout')"
                                        onclick="event.preventDefault();
                                                    this.closest('form').submit();">
                                    {{ __('Log Out') }}
                                </x-responsive-nav-link>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            @endauth

            @guest
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <x-responsive-nav-link class="block rounded-md px-3 py-2 text-base font-medium text-gray-400 hover:bg-gray-700 text-white hover:text-white" :href="route('login')">
                    {{ __('Login') }}
                </x-responsive-nav-link>
            </form>
            @endguest
        </div>
    </div>
</nav>
