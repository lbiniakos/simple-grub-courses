@extends('layouts.default')

@section('content')

@if ($courses->count() > 0)
<ul role="list" class="divide-y divide-gray-100">
    @foreach ($courses as $course)
        <x-profile-course-list-item :course="$course"/>
    @endforeach
</ul>
@else
    <p class="mt-6 text-lg leading-8 text-gray-600 mb-4">{{ __('You haven\'t enrolled on any course yet.') }}</p>
    <x-nav-link class="bg-indigo-600 text-white mt-4" :href="route('courses')">{{ __('Check Courses page') }}</x-primary-button>
@endif

@endsection