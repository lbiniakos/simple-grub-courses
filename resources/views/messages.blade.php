@if ($errors->any())
<div class="rounded-md bg-red-50 px-2 py-1 text-xs font-medium text-red-700 ring-1 ring-inset ring-red-600/10 mb-6">
    <ul class="text-sm text-red-600 space-y-1">
        {!! implode('', $errors->all('<li>:message</li>')) !!}
    </ul>
</div>
@endif

@if(session()->has('success'))
<div class="rounded-md bg-green-50 px-2 py-1 text-xs font-medium text-green-700 ring-1 ring-inset ring-green-600/20 mb-6">
    <ul>
        {{ session()->get('success') }}
    </ul>
</div>
@endif