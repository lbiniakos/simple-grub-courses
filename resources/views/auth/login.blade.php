<x-guest-layout>
    <div class="flex min-h-full flex-col justify-center px-6 py-12 lg:px-8">
        <div class="sm:mx-auto sm:w-full sm:max-w-sm">
        <img class="mx-auto h-10 w-auto" src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600" alt="Your Company">
        <h2 class="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">{{ __('Sign in to your account') }}</h2>
        </div>
    
        <div class="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
        <form method="POST" action="{{ route('login') }}" class="space-y-6">
            @csrf

            <div>
            <div class="mt-2">
                <x-input-label for="email" :value="__('Email address')" />
                <x-text-input id="email" type="email" name="email" :value="old('email')" required autofocus autocomplete="email" />
                <x-input-error :messages="$errors->get('email')" class="mt-2" />
            </div>
            </div>
    
            <div>
            <div class="flex items-center justify-between">
                <x-input-label  for="password" :value="__('Password')" />

                @if (Route::has('password.request'))
                <div class="text-sm">
                    <a class="font-semibold text-indigo-600 hover:text-indigo-500" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                </div>
                @endif
            </div>

            <div class="mt-2">
                <x-text-input id="password" type="password" name="password" :value="old('password')" required autofocus autocomplete="password" />
                <x-input-error :messages="$errors->get('password')" class="mt-2" />
            </div>
            </div>
    
            <div>
                <x-primary-button class="ms-3">
                    {{ __('Log in') }}
                </x-primary-button>
            </div>
        </form>
    
        <p class="mt-10 text-center text-sm text-gray-500">
            {{ __('Not having an account yet?') }}
            <a href="{{ route('register') }}" class="font-semibold leading-6 text-indigo-600 hover:text-indigo-500">{{ __('Create your account here.') }}</a>
        </p>
        </div>
    </div>
</x-guest-layout>  
