@extends('layouts.default')

@section('content')

<x-header>
    <x-heading-1>{{ __('About Us') }}</x-heading-1>
    <x-subheading>{{ __('Quisque laoreet elit et metus suscipit, a viverra elit gravida') }}</x-subheading>
</x-header>

<p class="mb-2">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque laoreet elit et metus suscipit, a viverra elit gravida. Cras eget augue at sem ullamcorper mattis non ac tortor. Vivamus ornare mi semper nibh ultrices condimentum. Vivamus sed mi sit amet nisi lacinia accumsan. Praesent nec velit at ex accumsan viverra sed a quam. Curabitur sed viverra justo. Mauris ut ornare diam. In hac habitasse platea dictumst. Ut non porta nisl. Suspendisse molestie aliquam ultrices. Duis efficitur aliquam pellentesque. Mauris semper, leo eget consequat dictum, neque sapien lacinia nibh, ac laoreet enim dolor eget massa. Etiam posuere ornare interdum. Duis non augue mauris. Aliquam erat volutpat.
</p>

<p class="mb-2">
    Curabitur eu metus odio. Proin malesuada id felis vel ullamcorper. Proin quis tortor sed lacus finibus mollis. Cras gravida ex scelerisque tortor dignissim mollis. Aliquam erat volutpat. Vestibulum malesuada rutrum arcu vitae auctor. Proin nec felis mi. Nam suscipit sapien et volutpat dignissim. Phasellus ac velit mollis, fermentum mi vel, lobortis turpis. Fusce in nibh orci. Quisque consequat, urna id condimentum egestas, ligula erat accumsan dui, iaculis pretium dolor ante sit amet augue.
</p>

<p class="mb-2">
    Aenean malesuada cursus purus eu cursus. Mauris in tortor urna. Vestibulum nec quam rutrum, interdum urna sed, tempus turpis. Ut rutrum, velit id lacinia blandit, ligula tortor feugiat elit, ut pharetra dolor lectus eget eros. Integer at posuere libero. Nulla sollicitudin enim et dui malesuada vehicula. Fusce commodo ligula in purus interdum commodo. Maecenas efficitur porta mauris vel volutpat.
</p>

<p class="mb-2">
    Quisque tincidunt mauris nisi, et fermentum ex molestie vel. Morbi erat ante, auctor sit amet tincidunt id, interdum vel ligula. Sed dapibus, orci nec luctus semper, enim nulla sodales leo, at mollis metus enim at dolor. Aenean at risus sed libero iaculis rhoncus vitae viverra mauris. Pellentesque eu erat nec nunc volutpat tristique. Maecenas condimentum aliquam consectetur. Vestibulum vestibulum luctus pellentesque.
</p>

<p class="mb-2">
    Quisque non orci dapibus, efficitur elit in, efficitur elit. Phasellus ut lorem ultricies, ornare sem eget, ullamcorper ligula. Vivamus sed nunc sagittis, scelerisque arcu eu, volutpat nisl. Curabitur fermentum volutpat lorem non ornare. Maecenas finibus felis felis, non elementum felis placerat et. Aenean id sem lacus. Integer varius ipsum non arcu placerat auctor. 
</p>
@endsection