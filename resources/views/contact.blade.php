@extends('layouts.default')

@section('content')

<x-header>
    <x-heading-1>{{ __('Contact Us') }}</x-heading-1>
    <x-subheading>{{ __('Quisque laoreet elit et metus suscipit, a viverra elit gravida') }}</x-subheading>
</x-header>

<!--
  This example requires some changes to your config:
  
  ```
  // tailwind.config.js
  module.exports = {
    // ...
    plugins: [
      // ...
      require('@tailwindcss/forms'),
    ],
  }
  ```
-->
<div class="isolate py-24">
    <form action="{{ route('send-email') }}" method="POST" class="mx-auto max-w-xl">
    @csrf
      <div class="grid grid-cols-1 gap-x-8 gap-y-6 sm:grid-cols-2">

        <div>
            <x-input-label for="firstname">{{ __('First name') }}</x-input-label>
            <x-text-input class="mt-2.5" type="text" name="firstname" id="firstname" :value="old('firstname')"/>
            <x-input-error :messages="$errors->get('firstname')" class="mt-2" />
        </div>

        <div>
            <x-input-label for="lastname">{{ __('Last name') }}</x-input-label>
            <x-text-input class="mt-2.5" type="text" name="lastname" id="lastname" :value="old('lastname')"/>
            <x-input-error :messages="$errors->get('lastname')" class="mt-2" />
        </div>

        <div class="sm:col-span-2">
          <x-input-label for="email">{{ __('Email') }}</x-input-label>
          <x-text-input class="mt-2.5" type="email" name="email" id="email" :value="old('email')"/>
          <x-input-error :messages="$errors->get('email')" class="mt-2" />
        </div>

        <div class="sm:col-span-2">
            <x-input-label for="phone">{{ __('Phone number') }}</x-input-label>
            <x-text-input class="mt-2.5" type="tel" name="phone" id="phone" :value="old('phone')"/>
            <x-input-error :messages="$errors->get('phone')" class="mt-2" />
        </div>

        <div class="sm:col-span-2">
          <x-input-label for="message">{{ __('Message') }}</x-input-label>
          <x-textarea  name="message" id="message" rows="4" class="mt-2.5">{{ old('message') }}</x-textarea>
          <x-input-error :messages="$errors->get('message')" class="mt-2" />
        </div>
       
      </div>

      <div class="mt-10">
        <x-primary-button class="w-full">{{ __('Let\'s talk') }}</x-primary-button>
      </div>

    </form>
</div>
  
@endsection