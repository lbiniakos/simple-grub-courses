@extends('layouts.default')

@section('content')

@if ($courses->count() > 0)
<ul role="list" class="divide-y divide-gray-100">
    @foreach ($courses as $course)
        <x-course-list-item :course="$course"/>
    @endforeach
</ul>

{{ $courses->links() }}
@else
<p class="mt-6 text-lg leading-8 text-gray-600 mb-4">{{ __('No courses found.') }}</p>
@endif
@endsection