@extends('layouts.default')

@section('content')

<form method="POST" action="{{ route('course-update') }}">
    @csrf
    <div class="space-y-12">
      <div class="">
        <h2 class="text-base font-semibold leading-7 text-gray-900">{{ __('Update Course: ') }} {{ $course->title}}</h2>
        
        <x-text-input type="hidden" name="id" value="{{ $course->id }}"/>
        <div class="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
          <div class="sm:col-span-4">
            <x-input-label for="title">{{ __('Title') }}</x-input-label>
            <x-text-input class="mt-2" id="title" name="title" required value="{{ $course->title }}"/>
          </div>
  
          <div class="col-span-full">
            <x-input-label for="description">{{ __('Description') }}</x-input-label>
            <x-textarea class="mt-2" id="description" rows="3" name="description" required>{{ $course->description }}</x-textarea>
            <p class="mt-3 text-sm leading-6 text-gray-600">{{ __('Write a few sentences about this course.') }}</p>
          </div>

          <div class="sm:col-span-4">
            <x-input-label for="total_slots">{{ __('Total slots for enrollment') }}</x-input-label>
            <x-text-input type="number" class="mt-2" id="total_slots" name="total_slots" value="{{ $course->total_slots }}" required/>
          </div>
  
        </div>
      </div>

    </div>
  
    <div class="mt-6 flex items-center justify-end gap-x-6">
      <x-primary-button>{{ __('Update Course') }}</x-primary-button>
    </div>
</form>
  
@endsection