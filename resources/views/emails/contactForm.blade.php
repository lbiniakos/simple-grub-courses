<x-mail::message>
# Name: {{ $firstname}} {{ $lastname }}<br/>
# Email: {{ $email }}<br/>
Phone: {{ $phone }}<br/>
Message: {{ $message }}

{{ config('app.name') }}
</x-mail::message>
