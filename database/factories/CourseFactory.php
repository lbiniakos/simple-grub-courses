<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Course>
 */
class CourseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->words(rand(5,15), true),
            'description' => $this->faker->paragraph(1),
            'status' => $this->faker->numberBetween(0,1),
            'enrolls' => $this->faker->randomDigit(),
            'total_slots' => $this->faker->numberBetween(10,20),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];
    }
}
