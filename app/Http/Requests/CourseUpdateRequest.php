<?php

namespace App\Http\Requests;

use App\Rules\SlotsGreaterThanEnrolls;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Rules;

class CourseUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
                'id' => 'required|exists:courses,id',
                'title' => 'required|string|min:4',
                'description' => 'required|string|max:250',
                'total_slots' => [
                    'required',
                    'integer',
                    'max:20',
                    'gt:0',
                    new SlotsGreaterThanEnrolls
                ]
        ];
    }
}
