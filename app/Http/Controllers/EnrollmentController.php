<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Enrollment;
use App\Models\User;
use App\Models\Course;
use Illuminate\Http\Request;

class EnrollmentController extends Controller
{
    public function enrollToCourse(Course $course, User $user)
    {
        if($course->enrolls >= $course->total_slots){

            return redirect()->route('courses')->withErrors(__('Course if full. Not any available slots.'));
        }

        $enroll = Enrollment::updateOrCreate([
            'course_id' => $course->id,
            'user_id' => $user->id
            ],
            [
                'course_id' => $course->id,
                'user_id' => $user->id
            ]
        );

        if($enroll->wasRecentlyCreated){

                $course->enrolls =  (int) $course->enrolls + 1;
                $course->save();

                return redirect()->route('courses')->with('success', __('You have successfully enrolled into course: ' . $course->title));
        }

        return redirect()->route('courses')->withErrors(__('You have already enrolled into this course.'));

    }

    public function unenrollFromCourse(Course $course, User $user)
    {
        $enroll = Enrollment::where([
            ['course_id', '=', $course->id],
            ['user_id', '=', $user->id]
        ])->first();

        if($enroll->delete()){

            $course->enrolls =  (int) $course->enrolls - 1;
            $course->save();

            return redirect()->route('profile.courses')->with('success', __('You have successfully unenrolled from course: ' . $course->title));
        }
    }
}
