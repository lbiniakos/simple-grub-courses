<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactFormRequest;
use App\Mail\ContactFormEmail;
use App\Models\ContactForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        return view('contact');
    }

    public function sendEmail(ContactFormRequest $request)
    {

        $contactForm = new ContactForm;
        $contactForm->fill($request->except('_token'));

        Mail::to('email@email.com')->send(new ContactFormEmail($contactForm));

        if( count(Mail::failures()) > 0 ) {

             return redirect()->back()->withErrors(Mail::failures());
        }

        return redirect()->back()->with('success', __('Your message was sent successfully!'));
    }
}
