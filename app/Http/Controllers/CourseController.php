<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CourseCreateRequest;
use App\Http\Requests\CourseUpdateRequest;
use App\Models\Course;
use App\Models\User;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function listing()
    {
        $courses = Course::select(['id', 'title', 'description', 'status', 'total_slots', 'enrolls','created_at'])->paginate(6);

        $data = [
            'courses' => $courses
        ];

        return view('courses.listing', $data);
    }

    public function create(CourseCreateRequest $request)
    {
        $course = Course::create($request->validated());
        
        if($course){

            return redirect()->back()->with('success', __('Course successfully created!'));
        }
    }

    public function editForm($courseId, Request $request)
    {
        $course = Course::find($courseId);

        if(!$course){

            return redirect()->route('courses')->withErrors(__('Not course with id:'. $courseId .' found'));
        }

        $data = [
            'course' => $course
        ];

        return view('courses.edit', $data);
    }

    public function update(CourseUpdateRequest $request)
    {
        $course = Course::where('id', $request->id)->update($request->validated());
        
        if($course){

            return redirect()->back()->with('success', __('Course successfully updated!'));
        }
    }

    public function delete(Request $request)
    {
        $course = Course::find($request->id);

        if(!$course){

            return redirect()->back()->withErrors(__('Not course with id:'. $request->id .' found'));
        }

        if($course->delete()){

            return redirect()->back()->with('success', __('Course successfully deleted!'));
        }
    }

    public function enroll($courseId, Request $request)
    {
        $course = Course::find($courseId);

        if(!$course){

            return redirect()->back()->withErrors(__('Not course with id:'. $courseId .' found'));
        }

        $user = User::find(auth()->user()->id);
        $ec = new EnrollmentController();
        return $ec->enrollToCourse($course, $user);
        
    }

    public function unenroll($courseId, Request $request)
    {
        $course = Course::find($courseId);

        if(!$course){

            return redirect()->back()->withErrors(__('Not course with id:'. $courseId .' found'));
        }

        $user = User::find(auth()->user()->id);
        $ec = new EnrollmentController();
        return $ec->unenrollFromCourse($course, $user);
        
    }
}

