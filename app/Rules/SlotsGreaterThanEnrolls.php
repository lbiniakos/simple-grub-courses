<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use App\Models\Course;

class SlotsGreaterThanEnrolls implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $course = Course::find(request()->id);

        if($course->enrolls > $value){
            $fail('The :attribute can\'t be lower than current total enrolls.');
        }
    }
}
