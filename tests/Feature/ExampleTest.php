<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Mail\ContactFormEmail;
use App\Models\ContactForm;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function test_the_application_returns_a_successful_response(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testBasicEmail()
    {
        $model = new ContactForm;
        $model->firstname = "firstname";
        $model->lastname = "lastname";
        $model->email = "email@email.com";
        $model->phone = "0123456789";
        $model->message = "This is a lorem ipsum";

        $mailable = new ContactFormEmail($model);
        $mailable->assertSeeInHtml('');
    }
}
