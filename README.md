# Laravel - Simple GRUB Course System

## Description

The Laravel GRUB System is a simple project designed for managing lessons in an educational setting. The system allows professors (admin) to create, edit, and delete lessons, while students can enroll and unenroll from these lessons. The application is built using the Laravel framework using tailwind. In order to

## Installation

1. **Clone the repository:**

   ```bash
   git clone [repository_url]

2. **Install dependencies:**

   ```bash
   composer install

3. **Configure the environment:**

Copy the .env.example file to .env and update the database and other relevant configurations.

- Generate the application key:
   ```bash
   php artisan key:generate

- Migrate the database:
   ```bash
   php artisan migrate

- (Optional) Seed the database with sample data:
   ```bash
   php artisan db:seed

4. **Run the development server:**
   ```bash
   php artisan serve

The application will be accessible at 127.0.0.1:8000.

## Usage
### Admin (Professor)

    Log in using admin credentials.
    Create, edit, or delete lessons.

### Student

    Log in using student credentials.
    View available lessons.
    Enroll and unenroll from lessons.
    View enrolled lessons.

### Features

    Lesson Management
        Create, edit, and delete lessons.
        View lesson details.

    User Roles
        Admin (Professor) with full control.
        Students with limited access.

    Enrollment System
        Students can enroll and unenroll from lessons.

## Contributing
We welcome contributions to improve the Laravel GRUB System. Feel free to submit issues, feature requests, or pull requests. Please follow our contribution guidelines.
License

The Laravel GRUB System is open-source software licensed under the MIT License.
Acknowledgments

Special thanks to the Laravel community and contributors for their valuable contributions to the ecosystem.